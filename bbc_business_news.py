#!/usr/bin/env python
# coding: utf-8
import re
import sys

import pipes_replacement


def is_main_bbc_news(it):
    title = it.find('title').text
    return re.match(r'Update:|Wall Street Update', title, re.I) is None

pipes_replacement.filter_feed(
    'http://www.bbc.co.uk/programmes/p02tb8vq/episodes/downloads.rss',
    is_main_bbc_news, sys.argv[1:])

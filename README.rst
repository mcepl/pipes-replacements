Pipes replacement
=================

This is a simple library with two examples of scripts using it
intended as a replacement for Yahoo! Pipes which are going to
die_ on September 30th, 2015.

Creation of the new filter is simple (see examples in the repo):

1. ``import pipes_replacement``
2. define a function which accepts RSS element as a parameter (as an ``Element`` from ElementTree_ ) and returns boolean indicating whether the item should be included in the output feed.
3. call ``filter_feed`` with the URL of the original feed and your filtering function as parameters.

.. _die:
    http://pipes.yqlblog.net/post/120705592639/pipes-end-of-life-announcement

.. _ElementTree:
    https://docs.python.org/2.7/library/xml.etree.elementtree.html

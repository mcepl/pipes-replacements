#!/usr/bin/env python
import argparse
import logging
logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.INFO)
import os.path
import urllib2

from ConfigParser import SafeConfigParser
import xml.etree.ElementTree as ET  # noqa

CACHE_FILE_NAME = os.path.expanduser('~/.cache/pipes_replacement.cfg')

# Configure ns_map
try:
    register_namespace = ET.register_namespace
except AttributeError:
    def register_namespace(prefix, uri):
        ET._namespace_map[uri] = prefix

register_namespace('itunes', 'http://www.itunes.com/dtds/podcast-1.0.dtd')
WRONG_MEDIA_NS = 'http://search.yahoo.com/mrss'
RIGHT_MEDIA_NS = '%s/' % WRONG_MEDIA_NS
FEED_ROOT_URL = 'https://matej.ceplovi.cz/feeds/%s'
register_namespace('media', WRONG_MEDIA_NS)
register_namespace('atom', 'http://www.w3.org/2005/Atom')
ns_map = dict(((abbr, url) for url, abbr in ET._namespace_map.iteritems()))

class NotModifiedHandler(urllib2.BaseHandler):
    def http_error_304(self, req, fp, code, message, headers):
        addinfourl = urllib2.addinfourl(fp, headers, req.get_full_url())
        addinfourl.code = code
        return addinfourl


def get_cached_values(filename, url):
    cache = SafeConfigParser({'etag': None, 'last-modified': None})
    cache.read(filename)

    if cache.has_section(url):
        etag = cache.get(url, 'etag')
        last_modified = cache.get(url, 'last-modified')
    else:
        etag = None
        last_modified = None

    return etag, last_modified


def safe_cache(etag, last_modified, url, cachefilename):
    if (etag is not None) or (last_modified is not None):
        cache = SafeConfigParser()
        cache.read(cachefilename)

        if not cache.has_section(url):
            cache.add_section(url)

        if etag is not None:
            cache.set(url, 'etag', etag)

        if last_modified is not None:
            cache.set(url, 'last-modified', last_modified)

        with open(cachefilename, 'wb') as configfile:
            cache.write(configfile)


def get_safe_feed(url, force=False):
    etag, last_modified = get_cached_values(CACHE_FILE_NAME, url)

    req = urllib2.Request(url)

    if not force:
        if etag is not None:
            req.add_header("If-None-Match", etag)

        if last_modified is not None:
            req.add_header("If-Modified-Since", last_modified)

    opener = urllib2.build_opener(NotModifiedHandler())

    url_handle = opener.open(req)
    headers = url_handle.info()

    etag = headers.getheader('ETag', None)
    last_modified = headers.getheader('Last-Modified', None)

    safe_cache(etag, last_modified, url, CACHE_FILE_NAME)

    if hasattr(url_handle, 'code') and url_handle.code == 304:
        return None

    return url_handle, headers


def filter_feed(url, check_function, args, logo_url=None):
    arg_parser = argparse.ArgumentParser(description='Filter podcasts.')
    arg_parser.add_argument(
        '-f', '--force', action='store_true',
        help='Force download of the feed, ignoring cached values')
    arg_parser.add_argument('target_filename', nargs=1,
                            help='filename to store the result to')
    parsed_args = arg_parser.parse_args(args)
    logging.debug('parsed_args = %s', parsed_args)
    target_fn = parsed_args.target_filename[0]

    ret = get_safe_feed(url, force=parsed_args.force)
    logging.debug('ret = %s', ret)

    if ret is not None:
        tree = ET.parse(ret[0])
        channel = tree.find('.//channel')
        for item in list(channel.findall('item'))[::-1]:
            if not check_function(item):
                channel.remove(item)

        # Here we should fix the feed problems
        # See https://gitlab.com/mcepl/pipes-replacements/issues/
        # Fix <itunes:category> ... 'News' is deprecated
        news_el = tree.find('.//{%s}category' % ns_map['itunes'])
        if news_el is not None and news_el.attrib['text'] == 'News':
            news_el.set('text', 'News & Politics')

        # Add logo if missing
        icon_el = tree.find('.//{%s}image' % ns_map['itunes'])
        if icon_el is None and logo_url is not None:
            icon_el = ET.Element('{%s}image' % ns_map['itunes'],
                                 attrib={'href': logo_url})
            channel.insert(0, icon_el)

        # Add <atom:link rel='self'>
        if channel.find('{%s}link' % ns_map['atom']) is None:
            atom_self = ET.Element('{%s}link' % ns_map['atom'],
                                   attrib={
                                   'href': FEED_ROOT_URL % os.path.basename(target_fn),
                                   'rel': "self",
                                   'type': 'application/rss+xml'
                                   })
            channel.insert(0, atom_self)

        # Fix media namespace
        for item in tree.findall('.//*'):
            if item.tag.startswith('{%s}' % WRONG_MEDIA_NS):
                item.tag = item.tag.replace(WRONG_MEDIA_NS, RIGHT_MEDIA_NS)

        logging.debug('target filename = %s', target_fn)
        tree.write(target_fn, encoding='utf-8')

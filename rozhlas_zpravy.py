#!/usr/bin/env python
# coding: utf-8
from __future__ import print_function

import re
import sys

import pipes_replacement

VECERNI_RE = re.compile(ur'Radiožurnál:.*18:00')

def is_main_zpravy(it):
    title = it.find('title').text
    return VECERNI_RE.search(title) is not None

pipes_replacement.filter_feed('http://static.rozhlas.cz/news/podcast.rss',
                              is_main_zpravy, sys.argv[1:],
                              logo_url='https://matej.ceplovi.cz/feeds/CR_ikona.png')  # noqa
